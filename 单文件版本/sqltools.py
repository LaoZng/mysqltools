# !/usr/bin/env python
# coding=utf-8
# 功能包：数据库功能包单文件版本
# 作者：LaoZng

from pony.orm import *
import datetime

db = Database()
db.bind(
    provider='mysql',
    host='127.0.0.1',
    port=int('3306'),
    user='rootx',
    passwd='123456',
    db='TestData'
)


class Login_Log(db.Entity):
    # id = PrimaryKey(str)
    ip = Optional(str, nullable=True)
    addr = Optional(str, nullable=True)
    create_time = Optional(datetime.datetime, default=datetime.datetime.now())


db.generate_mapping()


# 数据库功能相关部分
@db_session
def update_test():
    pass

