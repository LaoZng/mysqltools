# !/usr/bin/env python
# coding=utf-8
# 功能包：服配置管理
# 作者：LaoZng

import configparser
import os


class Config:
    def __init__(self):
        # 加载配置
        self.conf = configparser.ConfigParser()
        env_type = os.environ.get("env_type", None)
        if env_type is None:
            print(os.path.dirname(__file__) + '/default.ini')
            self.conf.read(os.path.dirname(__file__) + '/default.ini')
        else:
            print(os.path.dirname(__file__) + '/default-' + env_type + '.ini')
            self.conf.read(os.path.dirname(__file__) + '/default-' + env_type + '.ini')


conf = Config().conf
