# !/usr/bin/env python
# coding=utf-8
# 功能包：数据库映射
# 作者：LaoZng

import datetime
from pony.orm import *
from config.data_source import db


class Mailuser(db.Entity):
    id = PrimaryKey(int, auto=True)
    user = Optional(str, nullable=True)
    pwd = Optional(str, nullable=True)
    smtppwd = Optional(str, nullable=True)

    # 若要同步创建时间使用这个即可，插入时不用传参
    # create_time = Optional(datetime.datetime, default=datetime.datetime.now())


db.generate_mapping()
