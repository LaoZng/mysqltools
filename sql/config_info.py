# !/usr/bin/env python
# coding=utf-8
# 功能包：数据库操作
# 作者：LaoZng

from sql.orm import *
from pymysql.converters import escape_string


# 插入数据
@db_session
def insert_data(user, pwd, smtppwd):
    # 调用 orm 映射函数传参即可直接插入数据，自增id以及自动更新的时间不需要传参
    Mailuser(user=user,
             pwd=pwd,
             smtppwd=smtppwd)


# # 查询数据
# @db_session
# def query_datas_all():
#     sql = "SELECT * FROM mailuser"
#     return Mailuser.select_by_sql(sql)
# 查询数据
@db_session
def query_datas_all():
    datas = Mailuser.select()
    return datas


# # 查询单个数据
# @db_session
# def query_data_by_id(id):
#     sql = "SELECT * FROM mailuser where id = '{}'".format(id)
#     return Mailuser.select_by_sql(sql)

# 查询单个数据
@db_session
def query_data_by_id(id):
    data = Mailuser.get(id)
    name = data.name
    return data


# 判断数据是否存在
@db_session
def exists_data_by_id(id):
    is_exists = Mailuser.exists(id=id)
    return is_exists


# 删除数据
@db_session
def delete_data_by_id(id):
    # get查询单个 select查询多个结果为list
    data = Mailuser.get(id=id)
    data.delete()
    print('删除{}数据成功'.format(id), '*' * 10)


# 更新数据
@db_session
def update_product_infos_by_id(id, pwd):
    # get查询单个 select查询多个结果为list
    data = Mailuser.get(id=id)
    commit()
    # 更新数据，记得提交！
    data.pwd = pwd
