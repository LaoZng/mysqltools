# 数据库工具包
    作者：LaoZng


<span id="top"></span>
# 使用方法
|||
|:---|:---|
|1.|[修改default.ini相关配置](#defatult.ini)|
|2.|[建立orm映射](#orm.py)|
|3.|[创建操作数据库函数](#config_info.py)|
|4.|[调用 3](./test.py)|

## file* config

<span id="defatult.ini"></span>
### defatult.ini [👉点击前往](./config/default.ini)


    配置文件(修改数据库配置即可)
### configure.py
    加载配置
### data_source.py
    读取配置

## file* sql

<span id="orm.py"></span>
### orm.py[👉点击前往](./sql/orm.py)
    数据库映射
    创建的函数名于数据库表名对应,首字母大写,继承db.Entity
|数据库表名|对应函数名|
|:---|:---|
|user|User|
|user_info|User_Info|
|userinfo|Userinfo|
    不同映射字段的写法 : 不为空 默认值等属性自行百度
| 字段类型 | 写法 |
|:---|:---|
|主键|PrimaryKey(str)|
|str|Optional(str, nullable=True)|
|int|Optional(int, nullable=True)|
|longstr|Optional(longstr, nullable=True)|
|time|Optional(Optional(datetime.datetime|
|自动更新time|Optional(Optional(datetime.datetime, default=datetime.datetime.now())|

<span id="config_info.py"></span>
### config_info.py[👉点击前往](./sql/config_info.py)
    数据库操作函数,需添加装饰器@db_session
    使用orm操作数据库，
    若返回结果的函数操作使用数据库，需要添加装饰器，或者开头使用with db_session
    否则需要使用原生数据库sql操作
    (详见py文件)
[回到顶部](#top)
